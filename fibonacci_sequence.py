# This calculator calculates the fibonacci sequence

first_numbers_of_fs = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811]

def calulate_next_num(num1, num2):
	return (num1 + num2)

def fibonacci_sequence(top):
	try:
		num_sequence = [0,1]

		for i in range(top):
			num_sequence.append(int(calulate_next_num(num_sequence[i],num_sequence[i+1])))
		return num_sequence
	except TypeError:
		print("Wrong type on input. Only integers allowed.")


# Tests
print(fibonacci_sequence(30))
print(first_numbers_of_fs)
print(len(first_numbers_of_fs))
print(fibonacci_sequence(len(first_numbers_of_fs)-2) == first_numbers_of_fs)